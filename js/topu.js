/**
 * User: Tim Wang
 * Date: 2012-10-19
 * Dependent:
 *      raphael: 生成图形，兼容个浏览器；
 *      jquery：DOM操作；
 *      jquery-ui：drag事件；
 *      bootstrap：页面主题；
 *      jeegoocontext：右键菜单；
 *      dynatree：树形菜单；
 * Function:
 *      1.支持cluster布局
 *      2.网元自定义图标、连线、名称、颜色、大小
 *      3.网元可拖拽
 *      4.网元名称太长，truncate功能，tooltip提示
 */

var Topu = {};

Topu.count_size = function(da, parent) {
    if(da.children == undefined || da.children.length == 0) {
        da.size = 1;
        return 1;
    } else {
        for(var i=0; i< da.children.length; i++) {
            if(da.size == undefined) da.size = 0;
            da.size = da.size + Topu.count_size(da.children[i], da);
        }
        return da.size;
    }
}

Topu.recount_size = function(da, parent) {
    da.size = 0;
    if(da.children == undefined || da.children.length == 0) {
        da.size = 1;
        return 1;
    } else {
        for(var i=0; i< da.children.length; i++) {
            if(da.size == undefined) da.size = 0;
            da.size = da.size + Topu.recount_size(da.children[i], da);
        }
        return da.size;
    }
}

/**
 * 网元类
 */
Topu.Node = function(option) {
    var node = new Object;
    var default_option = {
        id: "",
        type: "node",
        name: "Node",
        ip: "",
        img: "img/olt.png",
        size: 0,
        level: 1,
        parent: null,
        width: 24,
        height: 24,
        connections: []
    };
    $.extend(default_option, option);
    node.id = default_option.id;
    node.type = default_option.type;
    node.name = default_option.name;
    node.ip = default_option.ip;
    node.img = default_option.img;
    node.size = default_option.size;
    node.level = default_option.level;
    node.parent = default_option.parent;
    node.width = default_option.width;
    node.height = default_option.height;
    node.connections = default_option.connections;
    node.x = default_option.y;
    node.y = default_option.x;
    return node;
}

/**
 * 解析d3.js布局后的节点
 */
Topu.NodeParser = function() {
    var parser = new Object;
    parser.node_list = [];
    var count = function(data, parent) {
        //var node = Topu.Node({id: data.id, type: data.type, name: data.name, x: data.x, y:data.y});
        data.parent = parent;
        if(data.children == undefined || data.children.length == 0) {
            data.size = 1;
            //data.children = [];
            data.width = 24;
            data.height = 24;
            temp = data.x;
            data.x = data.y;
            data.y = temp;
            return 1;
        } else {
            for(var i=0; i< data.children.length; i++) {
                data.size = data.size + count(data.children[i], data);
            }
            data.width = 24;
            data.height = 24;
            temp = data.x;
            data.x = data.y;
            data.y = temp;
            //node.children = data.children
            //parser.node_list.push(node);
            return data.size;
        }
    }
    parser.parse = function(data) {
        count(data, null)
        //return this.node_list;
    }
    return parser;
}

Topu.toggle = function(d) {
    if (d.children) {
        d._children = d.children;
        d.children = null;
    } else {
        d.children = d._children;
        d._children = null;
    }
}

/**
 * 连线两网元对象,node和node.parent
 */
Raphael.fn.connection = function (node, line, bg) {
    if (!node || !node.parent) return;
    if(node.connections == undefined) {
        node.connections = []
    }
    if(node.parent.connections == undefined) {
        node.parent.connections = []
    }
    var from = node,
        to = node.parent;
    if( node.line) line = node.line;
    var bb1 = node,
        bb2 = node.parent,
        p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
            {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
            {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
            {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
            {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
            {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
            {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
            {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
        d = {}, dis = [];
    for (var i = 0; i < 4; i++) {
        for (var j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x),
                dy = Math.abs(p[i].y - p[j].y);
            if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }
    if (dis.length == 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
        y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
        x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
        y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    /* d3拉塞尔曲线
    var p0 = {x: (node.x+node.width/2), y: (node.y+node.height/2)},
        p3 = {x: (node.parent.x+node.width/2), y: (node.parent.y+node.height/2)},
        m = (p0.y + p3.y) / 2,
        p = [p0, {x: p0.x, y: m}, {x: p3.x, y: m}, p3];
    var path = ["M", p[0].x.toFixed(3), p[0].y.toFixed(3), "C", p[1].x.toFixed(3), p[1].y.toFixed(3), p[2].x.toFixed(3), p[2].y.toFixed(3), p[3].x.toFixed(3), p[3].y.toFixed(3)].join(",");
    */
    /*  直线
    var path = ["M", (node.x+node.width/2).toFixed(3), (node.y+node.height/2).toFixed(3), "L", (node.parent.x+node.width/2).toFixed(3), (node.parent.y+node.height/2).toFixed(3)].join(",");
    */
    if (line && line.line) {
        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
    } else {
        var color = typeof line == "string" ? line : "#77dd77";
        var result = {
            bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[1] || 3}),
            line: this.path(path).attr({stroke: color, fill: "none"}),
            from: from,
            to: to
        }
        result.update = function(x, y) {
            result.line.attr({x: x, y: y})
        }
        from.connections.push(result);
        to.connections.push(result);
        return result;
    }
};

Raphael.fn.reconnect = function(connection) {
    if (connection.line && connection.from && connection.to) {
        line = connection;
        obj1 = line.from.raphael;
        obj2 = line.to.raphael;
    }
    var bb1 = obj1[0].getBBox(),
        bb2 = obj2[0].getBBox(),
        p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
            {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
            {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
            {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
            {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
            {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
            {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
            {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
        d = {}, dis = [];
    for (var i = 0; i < 4; i++) {
        for (var j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x),
                dy = Math.abs(p[i].y - p[j].y);
            if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }
    if (dis.length == 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
        y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
        x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
        y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    /* d3拉塞尔曲线
    var bb1 = obj1.getBBox(),
        bb2 = obj2.getBBox();
    var p0 = {x: (bb1.x+bb1.width/2), y: (bb1.y+bb1.height/2)},
        p3 = {x: (bb2.x+bb1.width/2), y: (bb2.y+bb1.height/2)},
        m = (p0.y + p3.y) / 2,
        p = [p0, {x: p0.x, y: m}, {x: p3.x, y: m}, p3];
    var path = ["M", p[0].x.toFixed(3), p[0].y.toFixed(3), "C", p[1].x.toFixed(3), p[1].y.toFixed(3), p[2].x.toFixed(3), p[2].y.toFixed(3), p[3].x.toFixed(3), p[3].y.toFixed(3)].join(",");
    */
    /* 直线
    var path = ["M", (bb1.x + bb1.width/2).toFixed(3), (bb1.y + bb1.height/2).toFixed(3), "L", (bb2.x + bb2.width/2).toFixed(3), (bb2.y + bb2.height/2).toFixed(3)].join(",");
    */
    if (line && line.line) {
        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
    }
};

function move(dx, dy) {
    this.update(dx - (this.dx || 0), dy - (this.dy || 0));
    this.dx = dx;
    this.dy = dy;
}
function up() {
    this.dx = this.dy = 0;
}

Raphael.fn.node = function (node_obj, config) {
    var default_config = {
        width: 24,
        height: 24
    }
    $.extend(default_config, config);
    var raphael = this;
    var node = raphael.set();
    var icon = raphael.image("img/olt.png", node_obj.x, node_obj.y, default_config.width, default_config.height);
    icon.node.id = node_obj.type+"_"+ node_obj.id;
    if(node_obj.children || (node_obj._children != undefined && node_obj._children)) {
        var name = raphael.text(node_obj.x-2, node_obj.y + node_obj.height/2, truncate(node_obj.name,30)).attr({fill:"#000", "font-size":12, "title": node_obj.name,"text-anchor": "end"});
    } else {
        var name = raphael.text(node_obj.x + node_obj.width+2, node_obj.y + node_obj.height/2, truncate(node_obj.name,30)).attr({fill:"#000", "font-size":12, "title": node_obj.name,"text-anchor": "start"});
    }
    node.push(icon, name);
    node.connections = node_obj.connections;
    node[0].update = function (dx, dy) {
        var x = this.attrs.x + dx,
            y = this.attrs.y + dy;
        node[0].attr({x: x, y: y});
        if(node_obj.children || (node_obj._children != undefined && node_obj._children)) {
            node[1].attr({x: x-2, y: y+default_config.height/2});
        } else {
            node[1].attr({x: x+default_config.width+2, y: y+default_config.height/2});
        }
        for (var i = node.connections.length; i--;) {
            raphael.reconnect(node.connections[i]);
        }
        raphael.safari();
    }
    node[0].drag(move, up);
    node_obj.raphael = node;
    return node;
}


/**
迁移 d3.js中的布局算法
*/


///////////////////////////////////////////////////////////////////////////////////
/* 公用方法 */

function forEach(array, action) {
    for (var i = 0; i < array.length; i++) {
        action(array[i]);
    }
}

function reduce(combine, base, array) {
    forEach(array, function(element) {
        base = combine(base, element);
    });
    return base;
}

function map(func, array) {
    var result = [];
    forEach(array, function(elem) {
        result.push(func(elem));
        // 对于map，func函数一般只有一个参数，所以用func(elem)
    });
    return result;
}

function truncate(text, length) {
    return text.length > length ? text.substring(0,length)+'...' : text
}

///////////////////////////////////////////////////////////////////////////////////
/* d3 core code */

d3 = {};

d3.max = function(array, f) {
    var i = -1,
        n = array.length,
        a,
        b;
    if (arguments.length === 1) {
        while (++i < n && ((a = array[i]) == null || a != a)) a = undefined;
        while (++i < n) if ((b = array[i]) != null && b > a) a = b;
    } else {
        while (++i < n && ((a = f.call(array, array[i], i)) == null || a != a)) a = undefined;
        while (++i < n) if ((b = f.call(array, array[i], i)) != null && b > a) a = b;
    }
    return a;
};

////////////////////////////////////////////////////////////////////////////////////////////
/* d3 rebind code */

// Copies a variable number of methods from source to target.
d3.rebind = function(target, source) {
    var i = 1, n = arguments.length, method;
    while (++i < n) target[method = arguments[i]] = d3_rebind(target, source, source[method]);
    return target;
};

// Method is assumed to be a standard D3 getter-setter:
// If passed with no arguments, gets the value.
// If passed with arguments, sets the value and returns the target.
function d3_rebind(target, source, method) {
    return function() {
        var value = method.apply(source, arguments);
        return arguments.length ? target : value;
    };
}

///////////////////////////////////////////////////////////////////////////////////
/* d3 layout core code */

d3.layout = {};

function d3_layout_treeVisitAfter(node, callback) {
    function visit(node, previousSibling) {
        var children = node.children;
        if (children && (n = children.length)) {
            var child, previousChild = null, i = -1, n;
            while (++i < n) {
                child = children[i];
                visit(child, previousChild);
                previousChild = child;
            }
        }
        callback(node, previousSibling);
    }
    visit(node, null);
}

function d3_layout_treeSeparation(a, b) {
    return a.parent == b.parent ? 1 : 2;
}

/////////////////////////////////////////////////////////////////////////////////////////
/* d3 layout hierarchy code */

d3.layout.hierarchy = function() {
    var sort = d3_layout_hierarchySort,
        children = d3_layout_hierarchyChildren,
        value = d3_layout_hierarchyValue;

    // Recursively compute the node depth and value.
    // Also converts to a standard hierarchy structure.
    function recurse(node, depth, nodes) {
        var childs = children.call(hierarchy, node, depth);
        node.depth = depth;
        nodes.push(node);
        if (childs && (n = childs.length)) {
            var i = -1,
                n,
                c = node.children = [],
                v = 0,
                j = depth + 1,
                d;
            while (++i < n) {
                d = recurse(childs[i], j, nodes);
                d.parent = node;
                c.push(d);
                v += d.value;
            }
            if (sort) c.sort(sort);
            if (value) node.value = v;
        } else if (value) {
            node.value = +value.call(hierarchy, node, depth) || 0;
        }
        return node;
    }

    // Recursively re-evaluates the node value.
    function revalue(node, depth) {
        var children = node.children,
            v = 0;
        if (children && (n = children.length)) {
            var i = -1,
                n,
                j = depth + 1;
            while (++i < n) v += revalue(children[i], j);
        } else if (value) {
            v = +value.call(hierarchy, node, depth) || 0;
        }
        if (value) node.value = v;
        return v;
    }

    function hierarchy(d) {
        var nodes = [];
        recurse(d, 0, nodes);
        return nodes;
    }

    hierarchy.sort = function(x) {
        if (!arguments.length) return sort;
        sort = x;
        return hierarchy;
    };

    hierarchy.children = function(x) {
        if (!arguments.length) return children;
        children = x;
        return hierarchy;
    };

    hierarchy.value = function(x) {
        if (!arguments.length) return value;
        value = x;
        return hierarchy;
    };

    // Re-evaluates the `value` property for the specified hierarchy.
    hierarchy.revalue = function(root) {
        revalue(root, 0);
        return root;
    };

    return hierarchy;
};

// A method assignment helper for hierarchy subclasses.
function d3_layout_hierarchyRebind(object, hierarchy) {
    d3.rebind(object, hierarchy, "sort", "children", "value");

    // Add an alias for nodes and links, for convenience.
    object.nodes = object;
    object.links = d3_layout_hierarchyLinks;

    return object;
}

function d3_layout_hierarchyChildren(d) {
    return d.children;
}

function d3_layout_hierarchyValue(d) {
    return d.value;
}

function d3_layout_hierarchySort(a, b) {
    return b.value - a.value;
}

// Returns an array source+target objects for the specified nodes.
function d3_layout_hierarchyLinks(nodes) {
    return d3.merge(nodes.map(function(parent) {
        return (parent.children || []).map(function(child) {
            return {source: parent, target: child};
        });
    }));
}


/////////////////////////////////////////////////////////////////////////////
/* d3 layout cluster 算法 */

// Implements a hierarchical layout using the cluster (or dendrogram)
// algorithm.
d3.layout.cluster = function() {
    var hierarchy = d3.layout.hierarchy().sort(null).value(null),
        separation = d3_layout_treeSeparation,
        size = [1, 1]; // width, height

    function cluster(d, i) {
        var nodes = hierarchy.call(this, d, i),
            root = nodes[0],
            previousNode,
            x = 0;

        // First walk, computing the initial x & y values.
        d3_layout_treeVisitAfter(root, function(node) {
            var children = node.children;
            if (children && children.length) {
                node.x = d3_layout_clusterX(children);
                node.y = d3_layout_clusterY(children);
            } else {
                node.x = previousNode ? x += separation(node, previousNode) : 0;
                node.y = 0;
                previousNode = node;
            }
        });

        // Compute the left-most, right-most, and depth-most nodes for extents.
        var left = d3_layout_clusterLeft(root),
            right = d3_layout_clusterRight(root),
            x0 = left.x - separation(left, right) / 2,
            x1 = right.x + separation(right, left) / 2;

        // Second walk, normalizing x & y to the desired size.
        d3_layout_treeVisitAfter(root, function(node) {
            node.x = (node.x - x0) / (x1 - x0) * size[0];
            node.y = (1 - (root.y ? node.y / root.y : 1)) * size[1];
        });

        return nodes;
    }

    cluster.separation = function(x) {
        if (!arguments.length) return separation;
        separation = x;
        return cluster;
    };

    cluster.size = function(x) {
        if (!arguments.length) return size;
        size = x;
        return cluster;
    };

    return d3_layout_hierarchyRebind(cluster, hierarchy);
};

function d3_layout_clusterY(children) {
    return 1 + d3.max(children, function(child) {
        return child.y;
    });
}

function d3_layout_clusterX(children) {
    return reduce(function(x, child) {
        return x + child.x;
    }, 0, children) / children.length;
}

function d3_layout_clusterLeft(node) {
    var children = node.children;
    return children && children.length ? d3_layout_clusterLeft(children[0]) : node;
}

function d3_layout_clusterRight(node) {
    var children = node.children, n;
    return children && (n = children.length) ? d3_layout_clusterRight(children[n - 1]) : node;
}

///////////////////////////////////////////////////////////
/* d3 tree layout 算法 */

// Node-link tree diagram using the Reingold-Tilford "tidy" algorithm
d3.layout.tree = function() {
    var hierarchy = d3.layout.hierarchy().sort(null).value(null),
        separation = d3_layout_treeSeparation,
        size = [1, 1]; // width, height

    function tree(d, i) {
        var nodes = hierarchy.call(this, d, i),
            root = nodes[0];

        function firstWalk(node, previousSibling) {
            var children = node.children,
                layout = node._tree;
            if (children && (n = children.length)) {
                var n,
                    firstChild = children[0],
                    previousChild,
                    ancestor = firstChild,
                    child,
                    i = -1;
                while (++i < n) {
                    child = children[i];
                    firstWalk(child, previousChild);
                    ancestor = apportion(child, previousChild, ancestor);
                    previousChild = child;
                }
                d3_layout_treeShift(node);
                var midpoint = .5 * (firstChild._tree.prelim + child._tree.prelim);
                if (previousSibling) {
                    layout.prelim = previousSibling._tree.prelim + separation(node, previousSibling);
                    layout.mod = layout.prelim - midpoint;
                } else {
                    layout.prelim = midpoint;
                }
            } else {
                if (previousSibling) {
                    layout.prelim = previousSibling._tree.prelim + separation(node, previousSibling);
                }
            }
        }

        function secondWalk(node, x) {
            node.x = node._tree.prelim + x;
            var children = node.children;
            if (children && (n = children.length)) {
                var i = -1,
                    n;
                x += node._tree.mod;
                while (++i < n) {
                    secondWalk(children[i], x);
                }
            }
        }

        function apportion(node, previousSibling, ancestor) {
            if (previousSibling) {
                var vip = node,
                    vop = node,
                    vim = previousSibling,
                    vom = node.parent.children[0],
                    sip = vip._tree.mod,
                    sop = vop._tree.mod,
                    sim = vim._tree.mod,
                    som = vom._tree.mod,
                    shift;
                while (vim = d3_layout_treeRight(vim), vip = d3_layout_treeLeft(vip), vim && vip) {
                    vom = d3_layout_treeLeft(vom);
                    vop = d3_layout_treeRight(vop);
                    vop._tree.ancestor = node;
                    shift = vim._tree.prelim + sim - vip._tree.prelim - sip + separation(vim, vip);
                    if (shift > 0) {
                        d3_layout_treeMove(d3_layout_treeAncestor(vim, node, ancestor), node, shift);
                        sip += shift;
                        sop += shift;
                    }
                    sim += vim._tree.mod;
                    sip += vip._tree.mod;
                    som += vom._tree.mod;
                    sop += vop._tree.mod;
                }
                if (vim && !d3_layout_treeRight(vop)) {
                    vop._tree.thread = vim;
                    vop._tree.mod += sim - sop;
                }
                if (vip && !d3_layout_treeLeft(vom)) {
                    vom._tree.thread = vip;
                    vom._tree.mod += sip - som;
                    ancestor = node;
                }
            }
            return ancestor;
        }

        // Initialize temporary layout variables.
        d3_layout_treeVisitAfter(root, function(node, previousSibling) {
            node._tree = {
                ancestor: node,
                prelim: 0,
                mod: 0,
                change: 0,
                shift: 0,
                number: previousSibling ? previousSibling._tree.number + 1 : 0
            };
        });

        // Compute the layout using Buchheim et al.'s algorithm.
        firstWalk(root);
        secondWalk(root, -root._tree.prelim);

        // Compute the left-most, right-most, and depth-most nodes for extents.
        var left = d3_layout_treeSearch(root, d3_layout_treeLeftmost),
            right = d3_layout_treeSearch(root, d3_layout_treeRightmost),
            deep = d3_layout_treeSearch(root, d3_layout_treeDeepest),
            x0 = left.x - separation(left, right) / 2,
            x1 = right.x + separation(right, left) / 2,
            y1 = deep.depth || 1;

        // Clear temporary layout variables; transform x and y.
        d3_layout_treeVisitAfter(root, function(node) {
            node.x = (node.x - x0) / (x1 - x0) * size[0];
            node.y = node.depth / y1 * size[1];
            delete node._tree;
        });

        return nodes;
    }

    tree.separation = function(x) {
        if (!arguments.length) return separation;
        separation = x;
        return tree;
    };

    tree.size = function(x) {
        if (!arguments.length) return size;
        size = x;
        return tree;
    };

    return d3_layout_hierarchyRebind(tree, hierarchy);
};

function d3_layout_treeSeparation(a, b) {
    return a.parent == b.parent ? 1 : 2;
}

// function d3_layout_treeSeparationRadial(a, b) {
//   return (a.parent == b.parent ? 1 : 2) / a.depth;
// }

function d3_layout_treeLeft(node) {
    var children = node.children;
    return children && children.length ? children[0] : node._tree.thread;
}

function d3_layout_treeRight(node) {
    var children = node.children,
        n;
    return children && (n = children.length) ? children[n - 1] : node._tree.thread;
}

function d3_layout_treeSearch(node, compare) {
    var children = node.children;
    if (children && (n = children.length)) {
        var child,
            n,
            i = -1;
        while (++i < n) {
            if (compare(child = d3_layout_treeSearch(children[i], compare), node) > 0) {
                node = child;
            }
        }
    }
    return node;
}

function d3_layout_treeRightmost(a, b) {
    return a.x - b.x;
}

function d3_layout_treeLeftmost(a, b) {
    return b.x - a.x;
}

function d3_layout_treeDeepest(a, b) {
    return a.depth - b.depth;
}

function d3_layout_treeVisitAfter(node, callback) {
    function visit(node, previousSibling) {
        var children = node.children;
        if (children && (n = children.length)) {
            var child,
                previousChild = null,
                i = -1,
                n;
            while (++i < n) {
                child = children[i];
                visit(child, previousChild);
                previousChild = child;
            }
        }
        callback(node, previousSibling);
    }
    visit(node, null);
}

function d3_layout_treeShift(node) {
    var shift = 0,
        change = 0,
        children = node.children,
        i = children.length,
        child;
    while (--i >= 0) {
        child = children[i]._tree;
        child.prelim += shift;
        child.mod += shift;
        shift += child.shift + (change += child.change);
    }
}

function d3_layout_treeMove(ancestor, node, shift) {
    ancestor = ancestor._tree;
    node = node._tree;
    var change = shift / (node.number - ancestor.number);
    ancestor.change += change;
    node.change -= change;
    node.shift += shift;
    node.prelim += shift;
    node.mod += shift;
}

function d3_layout_treeAncestor(vim, node, ancestor) {
    return vim._tree.ancestor.parent == node.parent
        ? vim._tree.ancestor
        : ancestor;
}